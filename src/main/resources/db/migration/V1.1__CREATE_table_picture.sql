CREATE TABLE picture(
    id SERIAL PRIMARY KEY,
    width INTEGER,
    file_name VARCHAR(500),
    user_id INTEGER
)