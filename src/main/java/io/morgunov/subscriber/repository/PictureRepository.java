package io.morgunov.subscriber.repository;

import io.morgunov.subscriber.model.Picture;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PictureRepository extends CrudRepository<Picture, Long> {

    @Query(value = "SELECT * FROM public.picture p WHERE p.user_id = :userId", nativeQuery = true)
    List<Picture> getPictures(int userId);
}
