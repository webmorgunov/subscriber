package io.morgunov.subscriber;

import io.morgunov.subscriber.dto.ResizePictureDto;
import io.morgunov.subscriber.properties.RabbitMqProperties;
import io.morgunov.subscriber.receiver.PictureResizeReceiver;
import lombok.AllArgsConstructor;
import org.springframework.amqp.core.MessageProperties;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.amqp.rabbit.listener.adapter.MessageListenerAdapter;
import org.springframework.amqp.support.converter.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@AllArgsConstructor
public class RabbitMqConfiguration {

    private final RabbitMqProperties rabbitMqProperties;

    @Bean
    MessageListenerAdapter listenerAdapter(PictureResizeReceiver receiver) {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(receiver, "receiveMessage");
        Jackson2JsonMessageConverter messageConverter = new Jackson2JsonMessageConverter();
        ClassMapper classMapper = new DefaultJackson2JavaTypeMapper() {
            @Override
            public Class<?> toClass(MessageProperties properties) {
                return ResizePictureDto.class;
            }
        };
        messageConverter.setClassMapper(classMapper);
        messageListenerAdapter.setMessageConverter(messageConverter);
        return messageListenerAdapter;
    }

    @Bean
    SimpleMessageListenerContainer container(ConnectionFactory connectionFactory,
                                             MessageListenerAdapter listenerAdapter) {
        SimpleMessageListenerContainer container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(rabbitMqProperties.getQueueName());
        container.setMessageListener(listenerAdapter);
        return container;
    }
}
