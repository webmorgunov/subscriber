package io.morgunov.subscriber.receiver;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.morgunov.subscriber.dto.ResizePictureDto;
import io.morgunov.subscriber.service.PictureService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Slf4j
@AllArgsConstructor
public class PictureResizeReceiver {

    private final PictureService pictureService;

    public void receiveMessage(ResizePictureDto resizePictureDto) throws JsonProcessingException {
        try {
            pictureService.resize(resizePictureDto.getFileName(), resizePictureDto.getWidth(), resizePictureDto.getUserId());
        } catch (IOException e) {
            log.error("Error file resize", e);
        }
    }
}
