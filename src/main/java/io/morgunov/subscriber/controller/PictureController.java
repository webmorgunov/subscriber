package io.morgunov.subscriber.controller;

import io.morgunov.subscriber.dto.PictureContainerDto;
import io.morgunov.subscriber.service.PictureService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/picture")
@AllArgsConstructor
public class PictureController {

    private final PictureService pictureService;

    @GetMapping("/getByUserId/{userId}")
    public ResponseEntity<PictureContainerDto> getPictures(@PathVariable final Integer userId) {
        PictureContainerDto pictureContainer = pictureService.getPictures(userId);
        return ResponseEntity.ok(pictureContainer);
    }
}
