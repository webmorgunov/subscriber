package io.morgunov.subscriber.model;

import lombok.*;

import javax.persistence.*;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@Entity
@Getter
@Builder
public class Picture {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "id")
    @Builder.Default private Long id = 0L;

    @Column(name = "width")
    private int width;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "user_id")
    private int userId;
}
