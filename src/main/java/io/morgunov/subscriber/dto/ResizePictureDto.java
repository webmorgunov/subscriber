package io.morgunov.subscriber.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResizePictureDto {
    private int width;
    private String fileName;
    private int userId;
}
