package io.morgunov.subscriber.dto;

import io.morgunov.subscriber.model.Picture;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.List;

@Getter
@AllArgsConstructor
public class PictureContainerDto {

    private List<Picture> pictures;
}
