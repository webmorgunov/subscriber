package io.morgunov.subscriber.service;

import io.morgunov.subscriber.dto.PictureContainerDto;
import java.io.IOException;

public interface PictureService {
    void resize(String path, int width, int userId) throws IOException;
    PictureContainerDto getPictures(int userId);
}
