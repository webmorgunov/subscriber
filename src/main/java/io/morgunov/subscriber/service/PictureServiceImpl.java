package io.morgunov.subscriber.service;

import io.morgunov.subscriber.dto.PictureContainerDto;
import io.morgunov.subscriber.model.Picture;
import io.morgunov.subscriber.properties.PictureProperties;
import io.morgunov.subscriber.repository.PictureRepository;
import lombok.AllArgsConstructor;
import org.imgscalr.Scalr;
import org.springframework.stereotype.Service;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
@AllArgsConstructor
public class PictureServiceImpl implements PictureService {

    private final PictureProperties pictureProperties;

    private final PictureRepository pictureRepository;

    @Override
    public void resize(String fileName, int width, int userId) throws IOException {
        Path picturePath = Paths.get(pictureProperties.getFilePath(), fileName);
        BufferedImage originalImage = ImageIO.read(picturePath.toFile());
        BufferedImage scaledImage = Scalr.resize(originalImage, Scalr.Mode.FIT_TO_WIDTH, width, 0);

        String extension = fileName.substring(fileName.lastIndexOf('.') + 1);

        ImageIO.write(scaledImage, extension, picturePath.toFile());

        Picture picture = Picture.builder()
                .width(width)
                .fileName(fileName)
                .userId(userId)
                .build();
        pictureRepository.save(picture);

    }

    public PictureContainerDto getPictures(int userId) {
        return new PictureContainerDto(pictureRepository.getPictures(userId));
    }
}
